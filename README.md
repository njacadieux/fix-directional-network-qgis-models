# Fix directional network QGIS Models

Fix Directional Network v2.1 (QGIS 3.28), Check Geometries, model 1 of 3. 
Fix Directional Network v2.1 (QGIS 3.28), Check Network, model 2 of 3.
Fix Directional Network v2.1 (QGIS 3.28), Fix Network, model 3 of 3.
   
Is your directional linear network driving you crazy? These 3 interdependent models will help you find and fix a multitude of problems in your GIS network so that your network analysis (like shortest path) will work flawlessly without the need to use a topology tolerance to compensate for bad data. Basically, it will help you create a perfect line network! It makes a great complement to the topology checker plugin.
Model 1 aims to fix geometries and will:
-a) Find and show to the user all the invalid geometries that need to be fixed before any GIS analysis.  (Some geometries must be fixed by the user, and some can be automatically corrected by the model.) An empty output layer means there are no errors found.
-b) Remove duplicate geometries.
-c) Remove duplicate nodes (vertices) (New in version 2).
-d) Find and show to the user all the multipart lines that have been converted to singlepart lines.
You may need to fix the geometries manually and re-run the model until you are happy with the results. Outputs from the first model are fed to model 2.

Model 2 aims to find potential network errors and will:

-a) Create a graph with nodes and edges (very useful for finding errors as all network analysis tools are ultimately performed on a graph).
-b) Find the following potential network errors: Changes in network vectorization direction, non-touching lines, crossing lines, closed lines, partially overlapping lines, parallel lines (edges) and lines that could be merged as one geometry.  Most of these errors must be fixed by the user before moving on to a network analysis. These are “potential” errors. For example, network vectorization direction may not be a problem if the line direction has no meaning in your network (ex: does not represent the direction of a river).  Empty output layers indicate that there are no potential errors found.
-c) Find all disconnected nodes (disconnected nodes represent lines that are not correctly snapped).
-d) Create a nearest neighbour analysis.  Filtering this layer can help you find, for example, all the nodes that are not snapped within a given distance (like <= 25 cm).   This analysis will give the user the power to fix the network only where it needs to be fixed (unlike when using a topology tolerance).  Float point errors smaller than e-10 appear as “0” in the “Distance” field of the nearest neighbour analysis files. By ordering the “Distance” field in numerical order, one can normally find a good starting point for the “maximum snapping radius” variable of model 3.

Model 3 aims to fix the network and will:

-a) Snap and split lines that are disconnected if they are within a user specified distance (see 2.d above).  Only the first or last nodes (vertices) of a line are moved.
-b) Show to the user what lines have been modified
-c) Check the output geometries for errors in case the algorithm has created invalid geometries (ex: zero length lines, duplicate nodes…) (New in version 2). An empty output layer means there are no errors found.
The fixed output network should then be re-run in model 2 to see what errors remain.
All models provide a way to track changes when modifications are done to the network file. The only exception to this rule is the deleted duplicate geometries and duplicate nodes of model 1. The original Feature ID is maintained to help the user merge old databases with the new geometries. The original input data is read but never modified.

Visit the project web site at https://gitlab.com/njacadieux/fix-directional-network-qgis-models and read “A Python Algorithm for Shortest-Path River Network Distance Calculations Considering River Flow Direction” for more details https://doi.org/10.3390/data5010008.

Watch my presentation of the model at the QGIS Open Day (2023-02-24) 
https://www.youtube.com/live/xmxPeAXLTx0?feature=share

Please go to https://plugins.qgis.org/models/ or to https://gitlab.com/njacadieux/fix-directional-network-qgis-models/ to download the models, test data or to file bug reports.  

    Built and tested on QGIS 3.28.2, seems to work with QGIS 3.22.10 LTR
